import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import GlobalContextProvider from './context/GlobalContextProvider';
import './styles/main.scss'

const rootElement = document.getElementById('root');

ReactDOM.render((
  <GlobalContextProvider>
    <App />
  </GlobalContextProvider>
), rootElement);