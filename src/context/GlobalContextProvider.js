import React, { Component } from 'react';
import randomid from 'randomid';

const GlobalContext = React.createContext();

class GlobalContextProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scale: 2,
      grid: [5, 5],
      blocks: [],
      selectedBlock: false,
      artboard: {
        size: [300, 200]
      }
    };

    this.addBlock = this.addBlock.bind(this);
    this.handleMouseDown = this.handleMouseDown.bind(this);
  }

  componentDidMount() {
    window.addEventListener('mousedown', e => this.handleMouseDown(e));
  }

  handleMouseDown(event) {
    const className = event.target.className;

    if (event && className.includes('block')) {
      this.setState({
        selectedBlock: event.srcElement.id
      });
    } else {
      this.setState({
        selectedBlock: false
      });
    }
  }

  useScale(factor) {
    return factor * this.state.scale;
  }

  setArtboardSize(width, height) {
    this.setState({
      artboard: {
        size: [width, height]
      }
    })
  }

  addBlock() {
    const randomId = 'block-' + randomid(16);
    const template = {
      id: randomId,
      size: [50, 50]
    };

    this.setState(prevState => ({
      blocks: [...prevState.blocks, template],
      selectedBlock: randomId
    }));
  }
  
  render() {
    return (
      <GlobalContext.Provider value={{
        state: this.state,
        useScale: this.useScale,
        addBlock: this.addBlock
      }}>
        {this.props.children}
      </GlobalContext.Provider>
    )
  }
}

export default GlobalContextProvider;
export { GlobalContext };