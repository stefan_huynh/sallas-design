import React, { Component } from 'react';
import { GlobalContext } from '../context/GlobalContextProvider';

export default class Grid extends Component {
  constructor(props) {
    super(props)
    this.canvas = React.createRef();

    this.draw = this.draw.bind(this);
  }

  componentDidMount() {
    this.artboardSize = [
      this.context.useScale(this.context.state.artboard.size[0]),
      this.context.useScale(this.context.state.artboard.size[1]),
    ];
    this.ctx = this.canvas.current.getContext('2d');
    this.gridSize = this.context.useScale(this.context.state.grid[0]);

    this.canvas.current.width = this.artboardSize[0];
    this.canvas.current.height = this.artboardSize[1];
    
    this.draw();
  }

  draw() {
    for (let x = 0; x < this.artboardSize[0]; x += this.gridSize) {
      this.ctx.beginPath();
      this.ctx.moveTo(x + .5, 0);
      this.ctx.lineTo(x + .5, this.artboardSize[1]);
      this.ctx.stroke();
    }

    for (let y = 0; y < this.artboardSize[0]; y += this.gridSize) {
      this.ctx.beginPath();
      this.ctx.moveTo(0, y + .5);
      this.ctx.lineTo(this.artboardSize[0], y + .5);
      this.ctx.stroke();
    }
  }
  
  render() {
    return (
      <canvas ref={this.canvas} className="grid" />
    )
  }
}
Grid.contextType = GlobalContext;