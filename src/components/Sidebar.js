import React, { Component } from 'react';
import { GlobalContext } from '../context/GlobalContextProvider';
import BlockOptions from './BlockOptions';

export default class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        {
          this.context.state.selectedBlock
          ? <BlockOptions />
          : ''
        }
      </div>
    )
  }
}
Sidebar.contextType = GlobalContext;