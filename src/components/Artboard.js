import React, { Component } from 'react';
import { GlobalContext } from '../context/GlobalContextProvider';
import Icon from './Icon';
import Block from './Block';
import Grid from './Grid';

export default class Artboard extends Component {  
  render() {
    const size = this.context.state.artboard.size;
    const style = {
      width: this.context.useScale(size[0]),
      height: this.context.useScale(size[1]),
    }
    const blocks = this.context.state.blocks;
    
    return (
      <div className="artboard" style={style}>
        {blocks.map((block, key) => (
          <Block
            key={key}
            id={block.id}
            size={block.size} />
        ))}
        <Grid />
      </div>
    )
  }
}
Artboard.contextType = GlobalContext;