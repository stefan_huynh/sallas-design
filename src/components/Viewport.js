import React, { Component } from 'react';
import Artboard from './Artboard';
import InsertMenu from './InsertMenu';

export default class Viewport extends Component {
  render() {
    return (
      <div className="viewport">
        <Artboard />
        <InsertMenu />
      </div>
    )
  }
}