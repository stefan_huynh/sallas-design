import React from 'react';
import Viewport from './Viewport';
import Navbar from './Navbar';
import Sidebar from './Sidebar';

const App = props => {
  return <div className="app">
    <Navbar />
    <Viewport />
    <Sidebar />
  </div>;
}
export default App;