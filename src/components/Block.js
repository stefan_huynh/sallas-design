import React, { Component } from 'react';
import { GlobalContext } from '../context/GlobalContextProvider';
import { Rnd } from 'react-rnd';
import * as Icon from 'react-feather';

export default class Block extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       name: '',
       isEditing: false
    }

    this.nameInput = React.createRef();

    this.toggleEditingMode = this.toggleEditingMode.bind(this);
  }

  toggleEditingMode() {
    this.setState(prevState => ({
      isEditing: !prevState.isEditing
    }), () => this.nameInput.current.focus());
  }
  
  render() {
    const isSelected = this.context.state.selectedBlock == this.props.id;
    
    const rndSettings = {
      default: {
        x: this.context.state.artboard.size[0] - this.props.size[0],
        y: this.context.state.artboard.size[1] - this.props.size[1],
        width: this.context.useScale(this.props.size[0]),
        height: this.context.useScale(this.props.size[1])
      },
      bounds: '.artboard',
      resizeGrid: [
        this.context.useScale(this.context.state.grid[0]),
        this.context.useScale(this.context.state.grid[1]),
      ],
      dragGrid: [
        this.context.useScale(this.context.state.grid[0]),
        this.context.useScale(this.context.state.grid[1]),
      ],
      resizeHandleClasses: {
        bottom: 'block__handle block__handle--bottom',
        bottomRight: 'block__handle block__corner-handle',
        bottomLeft: 'block__handle block__corner-handle',
        top: 'block__handle block__handle--top',
        topLeft: 'block__handle block__corner-handle',
        topRight: 'block__handle block__corner-handle',
        left: 'block__handle block__handle--left',
        right: 'block__handle block__handle--right',
      }
    };
    
    return (
      <div className="block-wrapper">
        <Rnd {...rndSettings}>
          <div
            className={'block' + (isSelected ? ' block--selected' : '')}
            id={this.props.id}>
              <div className="block__actions" style={{visibility: (isSelected ? 'visible' : 'hidden')}}>
                <input
                  ref={this.nameInput}
                  type="text"
                  className="block__name-input"
                  onMouseDown={e => e.stopPropagation()}
                  onChange={e => this.setState({name: e.target.value})}
                  hidden={!this.state.isEditing}
                />
                <div
                  className="block__action"
                  onClick={this.toggleEditingMode}
                >
                  <Icon.Edit3 size={20} />
                </div>
                <div
                  className="block__action"
                  onClick={this.toggleEditingMode}
                >
                  <Icon.Trash2 size={20} />
                </div>
                <div
                  className="block__action"
                  onClick={this.toggleEditingMode}
                >
                  <Icon.Copy size={20} />
                </div>
              </div>
              <div className="block__name">{this.state.name}</div>
            </div>
        </Rnd>
      </div>
    )
  }
}
Block.contextType = GlobalContext;