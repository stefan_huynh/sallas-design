import React, { Component } from 'react';
import blockTypes from '../api/blockTypes.json';
import Icon from './Icon.js';
import { GlobalContext } from '../context/GlobalContextProvider.js';

export default class InsertMenu extends Component {
  render() {
    return (
      <div className="insert-menu">
        {blockTypes.map((type, key) => (
          <div
            key={key}
            className="insert-menu__item"
            onClick={this.context[type.action]}
          >
            <Icon icon={type.icon} />
          </div>
        ))}
      </div>
    )
  }
}
InsertMenu.contextType = GlobalContext;