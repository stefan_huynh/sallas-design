import React, { Component } from 'react';
import * as Icons from 'react-feather';

export default class Icon extends Component {
  render() {
    const TagName = Icons[this.props.icon];
    
    return (
      <TagName {...this.props} />
    )
  }
}