import React, { useContext } from 'react';
import { GlobalContext } from './context/GlobalContextProvider';

const useBlock = id => {
  const { blocks } = useContext(GlobalContext);

  return blocks.find(block => {
    return block.id == id;
  });
};

export {
  useBlock
};